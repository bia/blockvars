package plugins.danyfel80.geom;

import java.awt.Dimension;

import icy.plugin.abstract_.Plugin;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;
import vars.geom.VarDimension;

/**
 * Allows to define a 2D integer dimension.
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class DefineDimension extends Plugin implements Block {

	VarInteger varW;
	VarInteger varH;

	VarDimension varDimension;

	@Override
	public void declareInput(VarList inputMap) {
		varW = new VarInteger("Widht", 0);
		varH = new VarInteger("Height", 0);

		inputMap.add(varW.getName(), varW);
		inputMap.add(varH.getName(), varH);
	}

	@Override
	public void declareOutput(VarList outputMap) {
		varDimension = new VarDimension("Dimension");
		outputMap.add(varDimension.getName(), varDimension);
	}

	@Override
	public void run() {
		varDimension.setValue(new Dimension(varW.getValue(true), varH.getValue(true)));
	}

}
